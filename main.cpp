#include <iostream>
#include <opencv2/videoio.hpp>

#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>

using namespace std;
using namespace cv;


int main(int argc, char **argv) {
    std::cout << "Hello, world!" << std::endl;
    
   // std::vector<Mat> orb_descriptor_database;
    
    
    // open image file
    std::string im_fname_1 = "/tmp/test1.png";
    std::string im_fname_2 = "/tmp/test2.png";
    auto img1 = imread(im_fname_1, CV_LOAD_IMAGE_COLOR);
    auto img2 = imread(im_fname_2, CV_LOAD_IMAGE_COLOR);
    
    
    // find the keypoints, with ORB
    vector<KeyPoint> kp;
    Ptr<FeatureDetector> detector = ORB::create();
    //detector->detect(image, kp);
    
    
    // draw keypoints
    //Mat img_keypoints;
    //drawKeypoints(image, kp, img_keypoints);
    
    
    // compute descriptor on keypoints, with ORB
    //Ptr<DescriptorExtractor> descriptorExtractor = ORB::create();
    //Mat descriptors;
    //descriptorExtractor->compute(image, kp, descriptors);
    
    //imshow("keypoints orb", descriptors);
    //waitKey(0);
    
    
    // autre methode
    // cf code ici http://docs.opencv.org/3.1.0/dc/d16/tutorial_akaze_tracking.html#gsc.tab=0
    
    vector<KeyPoint> kp1, kp2;
    Mat desc1, desc2;
    detector->detectAndCompute(img1, noArray(), kp1, desc1);
    detector->detectAndCompute(img2, noArray(), kp2, desc2);
    
    vector< vector<DMatch> > matches, good_matches;
    Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce-Hamming");
    matcher->knnMatch(desc1, desc2, matches, 2);
    //const double nn_match_ratio = 0.8f; // Nearest-neighbour matching ratio
    const double nn_match_ratio = 0.8f; // Nearest-neighbour matching ratio
    vector<Point2f> matched1, matched2;

  //-- Quick calculation of max and min distances between keypoints
    double max_dist = 0; double min_dist = 100;
  for( int i = 0; i < desc1.rows; i++ )
  { 
    double dist = matches[i][0].distance;
    if( dist < min_dist ) 
      min_dist = dist;
    if( dist > max_dist ) 
      max_dist = dist;
  }

   double th = 1.5 * min_dist;
  for( int i = 0; i < desc1.rows; i++ )
  { 
    if( matches[i][0].distance < th )
     { good_matches.push_back( matches[i]); }
  }


    
    for(unsigned i = 0; i < matches.size(); i++) {
      if(matches[i][0].distance < nn_match_ratio * matches[i][1].distance) {
        matched1.push_back(kp1[matches[i][0].queryIdx].pt);
        matched2.push_back(kp2[matches[i][0].trainIdx].pt);
    }
    }
    const double ransac_thresh = 2.5f; // RANSAC inlier threshold
    Mat inlier_mask, homography;

    homography = findHomography(matched1, matched2,
                            RANSAC, ransac_thresh, inlier_mask);
    
    //  corners of obj1
    std::vector<Point2f> obj_corners(4);
    obj_corners[0] = cvPoint(0,0); obj_corners[1] = cvPoint( img1.cols, 0 );
    obj_corners[2] = cvPoint( img1.cols, img1.rows ); obj_corners[3] = cvPoint( 0, img1.rows );
    std::vector<Point2f> scene_corners(4);
    
      perspectiveTransform( obj_corners, scene_corners, homography);
//-- Draw lines between the corners (the mapped object in the scene - image_2 )
    Mat img_matches;
    drawMatches(img1, kp1, img2, kp2, good_matches, img_matches);
  line( img_matches, scene_corners[0] + Point2f( img1.cols, 0), scene_corners[1] + Point2f( img1.cols, 0), Scalar(0, 255, 0), 4 );
  line( img_matches, scene_corners[1] + Point2f( img1.cols, 0), scene_corners[2] + Point2f( img1.cols, 0), Scalar( 0, 255, 0), 4 );
  line( img_matches, scene_corners[2] + Point2f( img1.cols, 0), scene_corners[3] + Point2f( img1.cols, 0), Scalar( 0, 255, 0), 4 );
  line( img_matches, scene_corners[3] + Point2f( img1.cols, 0), scene_corners[0] + Point2f( img1.cols, 0), Scalar( 0, 255, 0), 4 );

    
    
   imshow("keypoints matching", img_matches);
    waitKey(0);
    
    return 0;
}
